import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import * as $ from "jquery";
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
   username: string;
   login_form: FormGroup;

  constructor(private http: HttpClient,private route: ActivatedRoute, private router: Router,private fb: FormBuilder) { 
    // this.http.get("https://vtim.chancoders.com/api/Users").subscribe((res)=>{
    //   console.log(res)
    // })
  }
  ngOnInit(): void {
  
    this.login_form = this.fb.group({
      username:[''],
      password: ['']
    });

    $(document).ready(function(){
      $('#goRight').on('click', function(){
        $('#slideBox').animate({
          'marginLeft' : '0'
        });
        $('.topLayer').animate({
          'marginLeft' : '100%'
        });
      });
      $('#goLeft').on('click', function(){
        $('#slideBox').animate({
          'marginLeft' : '50%'
        });
        $('.topLayer').animate({
          'marginLeft': '0'
        });
      });
    });
  }

//   onSubmit() {
//     console.log(this.loginForm.get('username').value);
// alert('asdasd');

//      console.log(this.username);
//   }
  login(value:any){
    console.log(value)
    localStorage.setItem('role', 'admin');

    this.router.navigate(['/dashboard']);

    
  }
}
